@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
            @foreach ($posts as $post)
                
            <div class="card">
                <div class="card-header">
                        <img src="{{$post->avatar}}" alt="" style="border-radius:50%;
                        width:40px;
                        margin-right:5px">
                        <a href="">{{$post->name}}</a>
                    </div>
                <div class="card-body">
                    <img src="{{$post->image}}" style="width: 728px;margin:-20px -20px;">
                </div>
                <div class="card-footer">
                    <b>{{$post->email}}</b>
                    <br>
                    {{$post->caption}}
                    </div>
            </div>
            <br>
            @endforeach
        </div>
    </div>
</div>
@endsection
